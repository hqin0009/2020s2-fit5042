package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	private Set<Property> list;
	public ComparePropertySessionBean() {
		list = new HashSet<>();
	}
	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		list.add(property);
	}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		for(Property p : list) {
			if(p.getPropertyId()==property.getPropertyId()) {
				list.remove(p);
				break;
			}
		}
	}

	@Override
	public int getBestPerRoom() {
		// TODO Auto-generated method stub
		Integer bestID=0;
		int numberOfRooms;
		double price;
		double bestPerRoom = 10000000000.00;
		for(Property p : list) {
			numberOfRooms = p.getNumberOfBedrooms();
			price = p.getPrice();
			if(price/numberOfRooms < bestPerRoom) {
				bestPerRoom = price/numberOfRooms;
				bestID = p.getPropertyId();
			}
		}
		return bestID;
	}

	@Override
	public CompareProperty create() throws CreateException, RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
